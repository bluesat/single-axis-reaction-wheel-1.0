# Single Axis Reaction Wheel 1.0

A proof of concept prototype for a reaction wheel. This should serve as a starting point for the design of the first iteration of the triple-axis reaction wheel. This repository contains the code used for the development and operation of the reaction wheel.

## Requirements:
A wheel that can:
Rotate clockwise and anti-clockwise
Rotate at variable speeds
An IMU that measures the speed of rotation
A microcontroller program that can rotate the wheel in the opposite direction of rotation at a proportional speed
A structure to mount the wheel and circuits (very rough!)
Uses batteries!

## Contributors
Olivia Yem
Sundeep Sarwana